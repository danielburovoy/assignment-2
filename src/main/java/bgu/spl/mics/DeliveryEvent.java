package bgu.spl.mics;


public class DeliveryEvent implements Event<DeliveryEvent> {

	
	private String senderName;
	private String customer_address;
	private int customer_distance;

    public DeliveryEvent(String senderName,String address, int diatance) {
        this.senderName = senderName;
        this.customer_address=address;
        this.customer_distance = diatance;
    }

    public String getSenderName() {
        return senderName;
    }

	public String getCustomer_address() {
		return customer_address;
	}


	public int getCustomer_distance() {
		return customer_distance;
	}


}
