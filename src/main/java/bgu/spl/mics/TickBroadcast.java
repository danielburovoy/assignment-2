package bgu.spl.mics;

public class TickBroadcast implements Broadcast {
	
	private String senderId;
	private int tick;

    public TickBroadcast(int tick) {
        this.tick=tick;
    }

    public String getSenderId() {
        return senderId;
    }
    
    public int getTick(){
    	return this.tick;
    }
    

}
