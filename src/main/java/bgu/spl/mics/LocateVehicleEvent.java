package bgu.spl.mics;

import bgu.spl.mics.application.passiveObjects.DeliveryVehicle;


public class LocateVehicleEvent implements Event<Future<DeliveryVehicle>> {

	
    private String senderName;
    private int to_destination;
    private String to_address;
	

    public LocateVehicleEvent(String senderName) {
    	this.senderName = senderName;
    	//this.to_destination=destination;
    	//this.to_address = address;
        
    }

    public String getSenderName() {
        return senderName;
    }
    public int getDestination(){
    	return to_destination;
    }
    public String getAddress() {
        return to_address;
    }
    

}
