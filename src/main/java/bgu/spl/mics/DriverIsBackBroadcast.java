package bgu.spl.mics;

import bgu.spl.mics.application.passiveObjects.DeliveryVehicle;

public class DriverIsBackBroadcast implements Broadcast {
	
	private DeliveryVehicle release_vehicle;
	
	public DriverIsBackBroadcast(DeliveryVehicle vehicle){
		
		this.setRelease_vehicle(vehicle);
		
	}

	public DeliveryVehicle getRelease_vehicle() {
		return release_vehicle;
	}

	public void setRelease_vehicle(DeliveryVehicle release_vehicle) {
		this.release_vehicle = release_vehicle;
	}

}
