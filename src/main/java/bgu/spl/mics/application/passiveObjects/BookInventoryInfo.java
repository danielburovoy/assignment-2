package bgu.spl.mics.application.passiveObjects;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Passive data-object representing a information about a certain book in the inventory.
 * You must not alter any of the given public methods of this class. 
 * <p>
 * You may add fields and methods to this class as you see fit (including public methods).
 */
public class BookInventoryInfo
{
	
//--------------------------------------------------------------------------------------------------
//	Fields
//--------------------------------------------------------------------------------------------------	
	
	private String bookTitle;
	private AtomicInteger amount;
	private int price;
	private Semaphore lock;
	
//--------------------------------------------------------------------------------------------------
//	Constructor
//--------------------------------------------------------------------------------------------------	
	public BookInventoryInfo(String title, int amount , int price ){
		bookTitle = title;
		this.amount = new AtomicInteger(amount);
		this.price = price;
		lock = new Semaphore(1,true);
	}
	
//--------------------------------------------------------------------------------------------------
//	getters & setters
//--------------------------------------------------------------------------------------------------	
	
	/**
     * Retrieves the title of this book.
     * <p>
     * @return The title of this book.   
     */
	public String getBookTitle() {
		return bookTitle;
	}

	/**
     * Retrieves the amount of books of this type in the inventory.
     * <p>
     * @return amount of available books.      
     */
	public int getAmountInInventory() {
		
		if(amount.get() <= 2)
		{
			try 
			{
				lock.acquire();
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		
		return amount.intValue();
	}

	/**
     * Retrieves the price for  book.
     * <p>
     * @return the price of the book.
     */
	public int getPrice() {
		return price;
	}
	
	public void bookTaken(){

		int current;
			 do {
				 current = amount.get();
			    }while (!amount.compareAndSet(current, current-1));
			
			lock.release();
	}
	
//--------------------------------------------------------------------------------------------------
//	methods
//--------------------------------------------------------------------------------------------------
	
	public boolean isAvailable() {
		return amount.get() > 0 ;
	}
}
