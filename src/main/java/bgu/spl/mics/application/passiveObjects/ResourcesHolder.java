package bgu.spl.mics.application.passiveObjects;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

import bgu.spl.mics.Future;

/**
 * Passive object representing the resource manager.
 * You must not alter any of the given public methods of this class.
 * <p>
 * This class must be implemented safely as a thread-safe singleton.
 * You must not alter any of the given public methods of this class.
 * <p>
 * You can add ONLY private methods and fields to this class.
 */
public class ResourcesHolder {
	
//--------------------------------------------------------------------------------------------------
//	Fields
//--------------------------------------------------------------------------------------------------
	
	
	private static class ResourcesHolderH 
	{
		private static ResourcesHolder instance = new ResourcesHolder();
	}
	
	private LinkedBlockingQueue<DeliveryVehicle> availableV;
	
	private LinkedBlockingQueue<Future<DeliveryVehicle>> unresolvesrequests;
	
	private Semaphore lock;
	
//--------------------------------------------------------------------------------------------------
//	Constructor
//--------------------------------------------------------------------------------------------------	
	
	private ResourcesHolder() {
		
		unresolvesrequests = new LinkedBlockingQueue<Future<DeliveryVehicle>>();
		availableV = new LinkedBlockingQueue<DeliveryVehicle>();
		lock = new Semaphore(1,true);
	}
	
	
	/**
     * Retrieves the single instance of this class.
     */
	public static ResourcesHolder getInstance() {

         return ResourcesHolderH.instance;
	}
	
	/**
     * Tries to acquire a vehicle and gives a future object which will
     * resolve to a vehicle.
     * <p>
     * @return 	{@link Future<DeliveryVehicle>} object which will resolve to a 
     * 			{@link DeliveryVehicle} when completed.   
     */
	public Future<DeliveryVehicle> acquireVehicle() {
				
			Future<DeliveryVehicle> futureV = new Future<DeliveryVehicle>();
			
			try 
			{
				lock.acquire();
				
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			
			DeliveryVehicle deliverV = availableV.poll();
			
			if(deliverV != null)
			{
				futureV.resolve(deliverV);
				System.out.println("Vehicle dispached");
		
			}
			else
			{
				unresolvesrequests.add(futureV);
				System.out.println("No vehicle available at this time, order will be issued when a delivery is complete");
				
			}
            lock.release();
			return futureV;
	}
	
	/**
     * Releases a specified vehicle, opening it again for the possibility of
     * acquisition.
     * <p>
     * @param vehicle	{@link DeliveryVehicle} to be released.
     */
	public void releaseVehicle(DeliveryVehicle vehicle) {
		
		try
		{
			lock.acquire();
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		
		if(unresolvesrequests.peek() != null)
		{
			unresolvesrequests.poll().resolve(vehicle);
			System.out.println("Vehicle went to another delivery round");
		}
		else
		{
			availableV.add(vehicle);
			System.out.println("Vehicle returned");
		}
		lock.release();

	}
	
	/**
     * Receives a collection of vehicles and stores them.
     * <p>
     * @param vehicles	Array of {@link DeliveryVehicle} instances to store.
     */
	public void load(DeliveryVehicle[] vehicles) {
				
		for(DeliveryVehicle vehicle : vehicles)
		{
			availableV.add(vehicle);
		}
	}

}
