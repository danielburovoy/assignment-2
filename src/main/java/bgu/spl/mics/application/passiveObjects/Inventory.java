package bgu.spl.mics.application.passiveObjects;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import bgu.spl.mics.application.BookStoreRunner;
import bgu.spl.mics.application.services.InventoryService;

import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Passive data-object representing the store inventory.
 * It holds a collection of {@link BookInventoryInfo} for all the
 * books in the store.
 * <p>
 * This class must be implemented safely as a thread-safe singleton.
 * You must not alter any of the given public methods of this class.
 * <p>
 * You can add ONLY private fields and methods to this class as you see fit.
 */

public class Inventory implements Serializable {

//--------------------------------------------------------------------------------------------------
//	Fields
//--------------------------------------------------------------------------------------------------
	
	private static class InventoryHolder 
	{
		private static Inventory instance = new Inventory();
	}
	private ConcurrentHashMap<String,BookInventoryInfo> inventoryMap;
	private Object lock; 

//--------------------------------------------------------------------------------------------------
//	Constructor
//--------------------------------------------------------------------------------------------------	
	
	private Inventory(){
		
		inventoryMap = new ConcurrentHashMap<String,BookInventoryInfo>();
		Object lock = new Object();
	}
	
//--------------------------------------------------------------------------------------------------
//	methods
//-------------------------------------------------------------------------------------------------	
	
	/**
     * Retrieves the single instance of this class.
     */
	public static Inventory getInstance() { //TODO make it threadsafe
		
		return InventoryHolder.instance;
	}
	
	
	/**
     * Initializes the store inventory. This method adds all the items given to the store
     * inventory.
     * <p>
     * @param inventory 	Data structure containing all data necessary for initialization
     * 						of the inventory.
     */
	public void load (BookInventoryInfo[] inventory ) {	
				
		for(BookInventoryInfo book : inventory )
		{
			
			try
			{
				this.inventoryMap.put(book.getBookTitle(),book);
			}
			catch(NullPointerException e)
			{

			}
			
		}
	}
	
	/**
     * Attempts to take one book from the store.
     * <p>
     * @param book 		Name of the book to take from the store
     * @return 	an {@link Enum} with options NOT_IN_STOCK and SUCCESSFULLY_TAKEN.
     * 			The first should not change the state of the inventory while the 
     * 			second should reduce by one the number of books of the desired type.
     */
	public OrderResult take (String book) { //TODO implement synch here for thread safety
		

		if(this.inventoryMap.containsKey(book))
		{
			if(inventoryMap.get(book).isAvailable())
			{
				inventoryMap.get(book).bookTaken();
				return OrderResult.SUCCSUCCESSFULLY_TAKEN;
			}
		}
		return OrderResult.NOT_IN_STOCK;
	}	
	
	
	/**
     * Checks if a certain book is available in the inventory.
     * <p>
     * @param book 		Name of the book.
     * @return the price of the book if it is available, -1 otherwise.
     */
	public int checkAvailabiltyAndGetPrice(String book) { //TODO add synch here, this methods' safety depends on other methods.
		
		if(this.inventoryMap.containsKey(book))
		{
			if(inventoryMap.get(book).isAvailable())
			{
				return inventoryMap.get(book).getPrice();
			}
		}
		return -1;
	}
	
	/**
     * 
     * <p>
     * Prints to a file name @filename a serialized object HashMap<String,Integer> which is a Map of all the books in the inventory. The keys of the Map (type {@link String})
     * should be the titles of the books while the values (type {@link Integer}) should be
     * their respective available amount in the inventory. 
     * This method is called by the main method in order to generate the output.
     */
	public void printInventoryToFile(String filename){
		
		Iterator<ConcurrentHashMap.Entry<String, BookInventoryInfo>> itr = inventoryMap.entrySet().iterator();
		
		FileOutputStream inventoryFile;
		try {
				inventoryFile = new FileOutputStream(filename);
				
				ObjectOutputStream inventoryOut = new ObjectOutputStream(inventoryFile);
				
				HashMap<String,Integer> invetorySerMap = new HashMap<String,Integer>();
				
				for(ConcurrentHashMap.Entry<String, BookInventoryInfo> entry : inventoryMap.entrySet())
				{
					invetorySerMap.put(entry.getKey(),entry.getValue().getAmountInInventory());
				}
				
				inventoryOut.writeObject(invetorySerMap);
				
				inventoryOut.close();
				
				inventoryFile.close();
				
			}catch (IOException e)		
			{
				e.printStackTrace();
			}
	}


	
	public int getAmountOf(String bookToCheck) {
		

		return inventoryMap.get(bookToCheck).getAmountInInventory();

	}
}
