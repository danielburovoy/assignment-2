package bgu.spl.mics.application.passiveObjects;

import java.io.Serializable;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class CreditCard implements Serializable {

	
	
//--------------------------------------------------------------------------------------------------
//	Fields
//--------------------------------------------------------------------------------------------------
	private int cardNumber;
	private AtomicInteger amount;
	private Semaphore lock;
	
//--------------------------------------------------------------------------------------------------
//	Constructor
//--------------------------------------------------------------------------------------------------
	public CreditCard(int cardNumber, int amount)
	{
		this.cardNumber = cardNumber;
		this.amount = new AtomicInteger(amount);
		lock = new Semaphore(1, true);
		
	}
	
//--------------------------------------------------------------------------------------------------
//	getters & setters
//--------------------------------------------------------------------------------------------------

	public int getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(int cardNumber) {
		this.cardNumber = cardNumber;
	}

	public int getAmount() {

		return amount.get();
	}

	public void setAmount(int amount) {
		   
		try
		{
			lock.acquire();
		} catch (InterruptedException e) 
		{

			e.printStackTrace();
		}
		for (;;) 
		   {
		        int current = this.amount.get();
		        int next = current - amount;
		        if (this.amount.compareAndSet(current, next))
		        {
		            this.amount.set(current);
		            break;
		        }
		   }
		   lock.release();
	}

	public void notEnoughToCharge() {

		lock.release();
		
	}
	
	
}
