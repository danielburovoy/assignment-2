package bgu.spl.mics.application.passiveObjects;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Passive object representing the store finance management. 
 * It should hold a list of receipts issued by the store.
 * <p>
 * This class must be implemented safely as a thread-safe singleton.
 * You must not alter any of the given public methods of this class.
 * <p>
 * You can add ONLY private fields and methods to this class as you see fit.
 */
public class MoneyRegister implements Serializable {
	
//--------------------------------------------------------------------------------------------------
//	Fields
//--------------------------------------------------------------------------------------------------
	private int order_counter;
	private static class MoneyRegisterHolder 
	{
		private static MoneyRegister instance = new MoneyRegister();
	}
	private CopyOnWriteArrayList<OrderReceipt> receipts;
	private String lock;
	
//--------------------------------------------------------------------------------------------------
//	Constructor
//--------------------------------------------------------------------------------------------------	
	
	private MoneyRegister() {
		receipts = new CopyOnWriteArrayList<OrderReceipt>();
		lock = new String();
		order_counter=0;

	}

//--------------------------------------------------------------------------------------------------
//	Singleton getInstance
//--------------------------------------------------------------------------------------------------
	
	/**
     * Retrieves the single instance of this class.
     */
	public static MoneyRegister getInstance() { //TODO make threadsafe

         return MoneyRegisterHolder.instance;
      }

//--------------------------------------------------------------------------------------------------
//	Methods
//--------------------------------------------------------------------------------------------------	
	
	/**
     * Saves an order receipt in the money register.
     * <p>   
     * @param r		The receipt to save in the money register.
     */
	public void file (OrderReceipt r) {
		r.setOrderID(order_counter);
		order_counter++;
		receipts.add(r);
	}
	
	
	/**
     * Retrieves the current total earnings of the store.  
     */
	public int getTotalEarnings() {
		

		int sum = 0;
		for(OrderReceipt orderR : receipts)
		{
			sum += orderR.getPrice();
		}
		
		return sum;
	}
	
	/**
     * Charges the credit card of the customer a certain amount of money.
     * <p>
     * @param amount 	amount to charge
     */
	public void chargeCreditCard(Customer c, int amount) {
		
		if(c.enoughToChargeCC(amount))
		{
			c.chargeCC(amount);
		}
		else{
			c.notEnoughToCharge();
		}
		
	}
	
	/**
     * Prints to a file named @filename a serialized object List<OrderReceipt> which holds all the order receipts 
     * currently in the MoneyRegister
     * This method is called by the main method in order to generate the output.. 
     */
	public void printOrderReceipts(String filename) {
	
		try {
			FileOutputStream recipetsFile = new FileOutputStream(filename);
			
			ObjectOutputStream recieptOut = new ObjectOutputStream(recipetsFile);
			
			recieptOut.writeObject(this.receipts);
			
			recieptOut.close();
			
			recipetsFile.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
