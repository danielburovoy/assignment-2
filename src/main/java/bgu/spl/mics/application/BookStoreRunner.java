package bgu.spl.mics.application;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;

import java.io.ObjectOutputStream;

import java.io.IOException;

import java.util.HashMap;

import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;

import javax.management.monitor.MonitorSettingException;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;

import bgu.spl.mics.MicroService;
import bgu.spl.mics.application.passiveObjects.*;
import bgu.spl.mics.application.services.*;
import javafx.util.Pair;

/** This is the Main class of the application. You should parse the input file, 
 * create the different instances of the objects, and run the system.
 * In the end, you should output serialized objects.
 */
public class BookStoreRunner {
    public static void main(String[] args) {
    	
    	long start = System.currentTimeMillis(); 
    	Inventory storeInv = Inventory.getInstance();
    	
    	ResourcesHolder Resources = ResourcesHolder.getInstance();
    	
    	MoneyRegister moneyReg = MoneyRegister.getInstance();
    	    	
    	Vector<Thread> threadV = new Vector<Thread>();
    		  	
		Customer[] custArray = initialize(storeInv,Resources,threadV,args);

    	boolean stillRunning = true;
    	
    	int threadAmountLeft = threadV.size();
    	
    	while(stillRunning)
    	{
    		
    		for(int i = 0 ; i < threadV.size() ; i++)
    		{
    			if(!(threadV.get(i).isAlive()))
    			{
    				threadAmountLeft --;
    				threadV.remove(i);
    				System.out.println("A thread has ended");

    			}
    			if(threadAmountLeft == 0)
    			{
    				stillRunning = false;
    				break;
    			}
    		}
    	}
    	
		serlialize(storeInv, moneyReg, custArray, args);

        long finish = System.currentTimeMillis();
        System.out.println(finish - start);
	
    }
    
    public static Customer[] initialize(Inventory storeInv , ResourcesHolder Resources, Vector<Thread> threadV,String []args)
    {
      	  	
    	try 
    	{
    		
    		Gson gson = new Gson();
			FileReader file = new FileReader(args[0]);
			JsonReader Jread = new JsonReader(file);
			JsonParser Jparser = new JsonParser();
			JsonElement Jelement = Jparser.parse(Jread);
			
			JsonObject Jobject = Jelement.getAsJsonObject();
			JsonArray JarrayInv = Jobject.getAsJsonArray("initialInventory");
			BookInventoryInfo[] initialInv = new BookInventoryInfo[JarrayInv.size()];
			
			for(int i = 0 ; i < JarrayInv.size() ; i++)
			{
				String bookTitle = JarrayInv.get(i).getAsJsonObject().get("bookTitle").getAsString();
				
				int amount = JarrayInv.get(i).getAsJsonObject().get("amount").getAsInt();
				
				int price = JarrayInv.get(i).getAsJsonObject().get("price").getAsInt();
				
				BookInventoryInfo tmpBook = new BookInventoryInfo(bookTitle, amount, price);
				
				initialInv[i] = tmpBook;
		
			}
			storeInv.load(initialInv);
			
			JsonArray JarrayResources = Jobject.getAsJsonArray("initialResources");
			JsonElement JelVic = JarrayResources.get(0);
			DeliveryVehicle[] initialV = gson.fromJson(JelVic.getAsJsonObject().getAsJsonArray("vehicles"), DeliveryVehicle[].class);
			Resources.load(initialV);
			
			JsonObject Jelservice = Jobject.getAsJsonObject("services");  
			
			int Jsell = Jelservice.get("selling").getAsInt();
			int JinvServ = Jelservice.get("inventoryService").getAsInt();
			int Jlogistic = Jelservice.get("logistics").getAsInt();
			int  Jresource = Jelservice.get("resourcesService").getAsInt();
			JsonArray Jcustomer = Jelservice.getAsJsonArray("customers");
			Customer[] custArray = new Customer[Jcustomer.size()];
			
			CountDownLatch latch = new CountDownLatch(Jsell+JinvServ+Jlogistic+Jresource+custArray.length);
			
			insertServices(Jsell, "SellingServices", threadV, latch);			
		
			insertServices(JinvServ , "InventoryService", threadV, latch);
			
			insertServices(Jlogistic, "LogisticsService", threadV,latch);
			
			insertServices(Jresource, "ResourceService", threadV,latch);
			
			custArray = initializeCustomers(threadV,Jcustomer,latch);

			for(Thread thread : threadV)
			{
				thread.start();
			}
			
			latch.await();

			JsonElement Jtime = Jelservice.get("time");
			
			initializeTimer(Jtime,latch);
			
			return custArray;

		
		} 
    	catch (FileNotFoundException e) 
    	{
			e.printStackTrace();
			return null;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		}
    	 	
    }
    
    private static void initializeTimer(JsonElement Jtime, CountDownLatch latch) {
		
    	int Jspeed = Jtime.getAsJsonObject().getAsJsonPrimitive("speed").getAsInt();
    	
    	int Jdur = Jtime.getAsJsonObject().getAsJsonPrimitive("duration").getAsInt();
    	
    	Thread threadT = new Thread((Runnable)new TimeService(Jspeed,Jdur,latch));
    	
    	threadT.start();
		
	}


	private static Customer[] initializeCustomers(Vector<Thread> threadV, JsonArray jcustomer, CountDownLatch latch) {
    	
		Customer[] custArray = new Customer[jcustomer.size()];;
		
		for(int i = 0 ; i < jcustomer.size() ; i++)

    	{
    		JsonElement cust = jcustomer.get(i);
    		
    		int Jid = cust.getAsJsonObject().getAsJsonPrimitive("id").getAsInt();
    		
    		String Jname = cust.getAsJsonObject().getAsJsonPrimitive("name").getAsString();
    		
    		String Jaddress = cust.getAsJsonObject().getAsJsonPrimitive("address").getAsString();
    		
    		int Jdist = cust.getAsJsonObject().getAsJsonPrimitive("distance").getAsInt();
    		
    		int Jcard = cust.getAsJsonObject().getAsJsonObject("creditCard").getAsJsonPrimitive("number").getAsInt();
    		
    		int Jamount = cust.getAsJsonObject().getAsJsonObject("creditCard").getAsJsonPrimitive("amount").getAsInt();
    		
    		JsonArray Jordersched = cust.getAsJsonObject().getAsJsonArray("orderSchedule");
    		
    		Vector<Pair<String, Integer>> orderschedule = new Vector<Pair<String, Integer>>();
    		  		
    		for (int j = 0 ; j < Jordersched.size() ; j++)
    		{
    			Integer tmpI = Jordersched.get(j).getAsJsonObject().getAsJsonPrimitive("tick").getAsInt();
    			
    			String tmpS = Jordersched.get(j).getAsJsonObject().getAsJsonPrimitive("bookTitle").getAsString();
    			
    			Pair<String, Integer> tmp = new Pair<>(tmpS,tmpI);
       			
    			orderschedule.add(tmp);
    		}
    	  		
    		Customer customer = new Customer(Jid , Jname , Jaddress , Jdist , Jcard , Jamount);
    		
    		custArray[i] = customer;
    		
    		threadV.addElement(new Thread((Runnable)new APIService(orderschedule , customer,latch)));
    	}
    	
    	return custArray;

    	
	}

	@SuppressWarnings("unchecked")
	private static <T> T serviceBuilder(String serviceName, int argument, CountDownLatch latch){
    	
    	switch(serviceName)
    	{
    	case "SellingServices" :
    		return (T) new SellingService(argument,latch);
    	case "InventoryService" :
    		return (T) new InventoryService(argument,latch);
    	case "LogisticsService" :
    		return (T) new LogisticsService(argument,latch);
    	case "ResourceService" :
    		return (T) new ResourceService(argument,latch);
    	default :
    		return null;
    	}
    }
    
    @SuppressWarnings("unused")
	private static void insertServices (int num, String name,Vector<Thread> threadV, CountDownLatch latch) {
    	
    	for(int i = 0 ; i < num ; i++)
    	{
    		threadV.add(new Thread((Runnable)serviceBuilder(name, num , latch)));
    	}
    	
    }

    private static void serlialize(Inventory inv , MoneyRegister moneyReg,Customer[] customerArray, String[] args) {

    	try {
			FileOutputStream customersFile = new FileOutputStream(args[1]);
			
			FileOutputStream registerFile = new FileOutputStream(args[4]);
			
			ObjectOutputStream registerOut = new ObjectOutputStream(registerFile);
				
			ObjectOutputStream customerOut = new ObjectOutputStream(customersFile);
			
			HashMap<Integer,Customer> customerMap = new HashMap<Integer,Customer>();
			
			for (Customer customer : customerArray)
			{
				customerMap.put(customer.getId(), customer);
			}
			
			customerOut.writeObject(customerMap);
			
			customerOut.close();
			
			customersFile.close();
			
			inv.printInventoryToFile(args[2]);
			
			moneyReg.printOrderReceipts(args[3]);
			
			registerOut.writeObject(moneyReg);
			
			registerOut.close();
			
			registerFile.close();
					
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   	
	}
  
}

