package bgu.spl.mics.application.services;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;

import bgu.spl.mics.DriverIsBackBroadcast;
import bgu.spl.mics.EndOfTimeBroadcast;
import bgu.spl.mics.Future;
import bgu.spl.mics.LocateVehicleEvent;
import bgu.spl.mics.MicroService;
import bgu.spl.mics.application.passiveObjects.DeliveryVehicle;
import bgu.spl.mics.application.passiveObjects.ResourcesHolder;

/**
 * ResourceService is in charge of the store resources - the delivery vehicles.
 * Holds a reference to the {@link ResourceHolder} singleton of the store.
 * This class may not hold references for objects which it is not responsible for:
 * {@link MoneyRegister}, {@link Inventory}.
 * 
 * You can add private fields and public methods to this class.
 * You MAY change constructor signatures and even add new public constructors.
 */
public class ResourceService extends MicroService{
    
	private ResourcesHolder resource_holder;
 	private List<Future<DeliveryVehicle>> flist;
 	private CountDownLatch latch;
 	
	public ResourceService(int id , CountDownLatch latch) {
		super("Resource Service "+id);
		this.resource_holder=ResourcesHolder.getInstance();
		flist = new CopyOnWriteArrayList<>();
		this.latch=latch;
	}

	@Override
	protected void initialize() {
         subscribeEvent(LocateVehicleEvent.class, locate -> {
        	
        	Future <DeliveryVehicle> fu = resource_holder.acquireVehicle(); 
        	flist.add(fu);
        	complete(locate, fu);

		});     
		
		subscribeBroadcast(DriverIsBackBroadcast.class, d_back -> {
        	
        	resource_holder.releaseVehicle(d_back.getRelease_vehicle());
			
		});
		
        subscribeBroadcast(EndOfTimeBroadcast.class, end_br -> {
        	
        	Iterator<Future<DeliveryVehicle>> it = flist.iterator();
        	while(it.hasNext()){
        		((Future<DeliveryVehicle>) it.next()).resolve(null);
        	}
        	terminate();
			
		});
		
       latch.countDown();
		
		
	}

}
