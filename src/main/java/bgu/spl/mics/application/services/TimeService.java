package bgu.spl.mics.application.services;

import bgu.spl.mics.EndOfTimeBroadcast;
import bgu.spl.mics.MicroService;
import bgu.spl.mics.TickBroadcast;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

/**
 * TimeService is the global system timer There is only one instance of this micro-service.
 * It keeps track of the amount of ticks passed since initialization and notifies
 * all other micro-services about the current time tick using {@link Tick Broadcast}.
 * This class may not hold references for objects which it is not responsible for:
 * {@link ResourcesHolder}, {@link MoneyRegister}, {@link Inventory}.
 * 
 * You can add private fields and public methods to this class.
 * You MAY change constructor signatures and even add new public constructors.
 */
public class TimeService extends MicroService{
	
	//fields
	private Timer _myTimer;
	private TimerTask _myTask;
	private int currentTick;
	private int _duration;
	private int _speed;

	public TimeService(CountDownLatch latch) {
		super("Change_This_Name");
		// TODO Implement this
	}
	
	public TimeService(int speed , int duration, CountDownLatch latch)
	{   
		super("System Timing Service");
		this._speed=speed;
		this._duration=duration;
		this._myTimer = new Timer();
		currentTick = 1; //initial tick value
		_myTask = new TimerTask(){
			 public void run() {				 
				 if (_duration>0){				
				 sendBroadcast(new TickBroadcast(currentTick));
				 currentTick++;	
				 
				 }		 
				 if (_duration==0){
					// synchronized (this) {					
                      sendBroadcast(new EndOfTimeBroadcast()); // TODO figure out what is needed at this stage					 
                      killTimer();		
                      
				 }//}
				 _duration--;
			 }
		};
	}
	@Override
	protected void initialize() {
		_myTimer.schedule(_myTask, 0, _speed);
		
        subscribeBroadcast(EndOfTimeBroadcast.class, end_br -> {
        	
       	terminate();
			
		});
				
		}
	
	public void killTimer(){
		_myTimer.cancel();

	}
	

}

