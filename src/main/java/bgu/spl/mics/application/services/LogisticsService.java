package bgu.spl.mics.application.services;

import java.util.concurrent.CountDownLatch;

import bgu.spl.mics.BookOrderEvent;
import bgu.spl.mics.DeliveryEvent;
import bgu.spl.mics.DriverIsBackBroadcast;
import bgu.spl.mics.EndOfTimeBroadcast;
import bgu.spl.mics.Future;
import bgu.spl.mics.LocateVehicleEvent;
import bgu.spl.mics.MicroService;

import bgu.spl.mics.application.passiveObjects.DeliveryVehicle;

/**
 * Logistic service in charge of delivering books that have been purchased to customers.
 * Handles {@link DeliveryEvent}.
 * This class may not hold references for objects which it is not responsible for:
 * {@link ResourcesHolder}, {@link MoneyRegister}, {@link Inventory}.
 * 
 * You can add private fields and public methods to this class.
 * You MAY change constructor signatures and even add new public constructors.
 */
public class LogisticsService extends MicroService {

	private int service_id;
	private CountDownLatch latch;
	
	public LogisticsService(int id, CountDownLatch latch) {
		super("Logistics Service "+id);
		this.latch=latch;
        service_id=id;
		// TODO Implement this
	}

	@Override
	protected void initialize() {
        System.out.println(getName() + " started");
       
        subscribeEvent(DeliveryEvent.class, deliver -> {
        Future <Future<DeliveryVehicle>> fu = (Future <Future<DeliveryVehicle>>)sendEvent(new LocateVehicleEvent(this.getName()));
		if(fu!=null){

			Future <DeliveryVehicle> fvehicle = fu.get();
			DeliveryVehicle vehicle=null;
			if (fvehicle!=null){
				vehicle=fvehicle.get();
			}
			if(vehicle!=null){
			vehicle.deliver(((DeliveryEvent) deliver).getCustomer_address(), ((DeliveryEvent) deliver).getCustomer_distance());
			sendBroadcast(new DriverIsBackBroadcast(vehicle));
			}
		}
	});
                
        subscribeBroadcast(EndOfTimeBroadcast.class, end_br -> {
        	
        	terminate();
			
		});
        

        latch.countDown();


}
}