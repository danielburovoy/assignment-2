package bgu.spl.mics.application.services;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;

import javafx.util.Pair;
import bgu.spl.mics.BookOrderEvent;
import bgu.spl.mics.EndOfTimeBroadcast;
import bgu.spl.mics.Future;
import bgu.spl.mics.MicroService;
import bgu.spl.mics.TickBroadcast;
import bgu.spl.mics.application.passiveObjects.Customer;
import bgu.spl.mics.application.passiveObjects.OrderReceipt;
import bgu.spl.mics.example.messages.ExampleBroadcast;
import bgu.spl.mics.application.passiveObjects.OrderReceipt;;

/**
 * APIService is in charge of the connection between a client and the store. It
 * informs the store about desired purchases using {@link BookOrderEvent}. This
 * class may not hold references for objects which it is not responsible for:
 * {@link ResourcesHolder}, {@link MoneyRegister}, {@link Inventory}.
 * 
 * You can add private fields and public methods to this class. You MAY change
 * constructor signatures and even add new public constructors.
 */
public class APIService extends MicroService {

	private Vector<Pair<String, Integer>> customerOrderSchedule;
	private Customer customer;
	private List<Future<OrderReceipt>> futureList;
	private CountDownLatch latch;

	public APIService(CountDownLatch latch) {
		super("Change_This_Name");

		// TODO Implement this
	}

	public APIService(Vector<Pair<String, Integer>> orderSchedule, Customer cstmr, CountDownLatch latch) {
		super("API");
		this.customer = cstmr;
		this.latch=latch;
		customerOrderSchedule = orderSchedule;
		futureList = new CopyOnWriteArrayList<>();
		customerOrderSchedule.sort(new Comparator<Pair<String, Integer>>() {
			public int compare(final Pair<String, Integer> p1, final Pair<String, Integer> p2) {
				int n1 = p1.getValue();
				int n2 = p2.getValue();
				return n1 < n2 ? -1 : n1 > n2 ? +1 : 0;
			}
		});

		// TODO Implement this
	}

	@Override
	protected void initialize() {

		System.out.println(getName() + " started");

		subscribeBroadcast(TickBroadcast.class, tick_br -> {
			while (!customerOrderSchedule.isEmpty()
					&& tick_br.getTick() == customerOrderSchedule.elementAt(0).getValue()) {
				Future<OrderReceipt> future = (Future<OrderReceipt>) sendEvent(
						new BookOrderEvent(this.customer, customerOrderSchedule.remove(0).getKey(), tick_br.getTick(),this.getName()));
				if (future != null)
					futureList.add(future);
			}
			for (Future<OrderReceipt> fu : futureList) {

				OrderReceipt reciept = fu.get();
				if (reciept != null) {
					
					customer.addToReceiptList(reciept);
				}
			}
			
		});
		
		
         subscribeBroadcast(EndOfTimeBroadcast.class, end_br -> {
            	
        	terminate();
			
		});
         
        latch.countDown();

	}

}
