package bgu.spl.mics.application.services;

import java.security.cert.CertPathChecker;
import java.util.concurrent.CountDownLatch;

import bgu.spl.mics.BookOrderEvent;
import bgu.spl.mics.CheckBookAvailability;
import bgu.spl.mics.EndOfTimeBroadcast;
import bgu.spl.mics.Future;
import bgu.spl.mics.MicroService;
import bgu.spl.mics.TickBroadcast;
import bgu.spl.mics.application.passiveObjects.BookInventoryInfo;
import bgu.spl.mics.application.passiveObjects.Inventory;
import bgu.spl.mics.application.passiveObjects.OrderReceipt;
import bgu.spl.mics.application.passiveObjects.OrderResult;

/**
 * InventoryService is in charge of the book inventory and stock.
 * Holds a reference to the {@link Inventory} singleton of the store.
 * This class may not hold references for objects which it is not responsible for:
 * {@link ResourcesHolder}, {@link MoneyRegister}.
 * 
 * You can add private fields and public methods to this class.
 * You MAY change constructor signatures and even add new public constructors.
 */

public class InventoryService extends MicroService{

	private Inventory my_inventory ;
	private CountDownLatch latch;
	public InventoryService(int id , CountDownLatch latch) {
		super("Inventory Service "+id);
		this.latch=latch;
		my_inventory = Inventory.getInstance();
		
	}

	@Override
	protected void initialize() {
		System.out.println(getName() + " started");
        
        subscribeEvent(CheckBookAvailability.class, check_book -> {
        	int price=my_inventory.checkAvailabiltyAndGetPrice(check_book.getBookToCheck());
        	if (price!=-1 && price<=check_book.getCustomerCredit()){
                 my_inventory.take(check_book.getBookToCheck());
                 complete(check_book,price);
        	}
        	else
        	{
        		
        		complete(check_book, null); // returns null if book does not exist or not enough money
        	}
            

        });
        
        subscribeBroadcast(EndOfTimeBroadcast.class, end_br -> {
        	
        	terminate();
			
		});
        
        latch.countDown();
		
	}

}
