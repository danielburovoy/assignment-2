package bgu.spl.mics.application.services;

import java.util.concurrent.CountDownLatch;

import bgu.spl.mics.BookOrderEvent;
import bgu.spl.mics.CheckBookAvailability;
import bgu.spl.mics.DeliveryEvent;
import bgu.spl.mics.EndOfTimeBroadcast;
import bgu.spl.mics.Future;
import bgu.spl.mics.MicroService;
import bgu.spl.mics.TickBroadcast;
import bgu.spl.mics.application.passiveObjects.BookInventoryInfo;
import bgu.spl.mics.application.passiveObjects.MoneyRegister;
import bgu.spl.mics.application.passiveObjects.OrderReceipt;


/**
 * Selling service in charge of taking orders from customers.
 * Holds a reference to the {@link MoneyRegister} singleton of the store.
 * Handles {@link BookOrderEvent}.
 * This class may not hold references for objects which it is not responsible for:
 * {@link ResourcesHolder}, {@link Inventory}.
 * 
 * You can add private fields and public methods to this class.
 * You MAY change constructor signatures and even add new public constructors.
 */
public class SellingService extends MicroService{

	private MoneyRegister mregister;
	private int service_id;
    private int current_tick;
    private CountDownLatch latch;
	
	public SellingService(int id, CountDownLatch latch) {
		
		super("Selling Service "+id);
		this.latch=latch;
		mregister = MoneyRegister.getInstance(); // retrieves the singleton instance
		this.service_id=id;
	}
	
	

	@Override
	protected void initialize() {
    System.out.println(getName() + " started");
        
        subscribeEvent(BookOrderEvent.class, book_ord -> {
        	// first check availability and see if customer have enough money
        	Future<Integer> info = (Future<Integer>)sendEvent(new CheckBookAvailability(this.getName(),book_ord.getBookName(),book_ord.customerCredit()));
        	if (info != null) { // would be null in case there is no selling service to handle the event
        		Integer resolved = info.get();//(100, TimeUnit.MILLISECONDS);
        		if (resolved!=null){ // null will be returned in case that book not in stock or not enough money
        			OrderReceipt r = new OrderReceipt(-1, this.getName(), book_ord.getCustomerID(),book_ord.getBookName(), resolved.intValue(), current_tick, book_ord.getOrderedTick(), current_tick); //TODO define order id        			
        			mregister.chargeCreditCard(book_ord.getCustomer(),r.getPrice());
        		    mregister.file(r);
        		    complete(book_ord,r);
        		    sendEvent(new DeliveryEvent(getName(), book_ord.getCustomerAddress(), book_ord.getCustomerDistance()));
        			
        		}
        		else{
        			complete(book_ord,null); // returns null toAPI if purchase unsuccessful
        			mregister.chargeCreditCard(book_ord.getCustomer(),Integer.MAX_VALUE);
        		}
        	}
        	
        });
        
        subscribeBroadcast(TickBroadcast.class, tick_br -> {
        	
        	this.current_tick = tick_br.getTick();
			
		});
        
        subscribeBroadcast(EndOfTimeBroadcast.class, end_br -> {
        	
        	terminate();
			
		});
        
        latch.countDown();
		
	}

}
