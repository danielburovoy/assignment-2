package bgu.spl.mics;
import java.util.Queue;
import java.util.Vector;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.hamcrest.core.IsInstanceOf;


/**
 * The {@link MessageBusImpl class is the implementation of the MessageBus interface.
 * Write your implementation here!
 * Only private fields and methods can be added to this class.
 */
public class MessageBusImpl implements MessageBus {
	
	private static MessageBusImpl instance = null;
	private ConcurrentHashMap <MicroService,BlockingQueue<Message>> queueMap; 
	private ConcurrentHashMap <Class,Queue<MicroService>> messageMap;
	//private ConcurrentHashMap <Class,Integer> currentRobinIndex;
	private ConcurrentHashMap <Event, Future> futureMap; 
	
	private MessageBusImpl(){
		queueMap = new ConcurrentHashMap<>();
		//currentRobinIndex = new ConcurrentHashMap<>();
		messageMap = new ConcurrentHashMap<>();
		futureMap = new ConcurrentHashMap<>();
	}

	@Override
	public <T> void subscribeEvent(Class<? extends Event<T>> type, MicroService m) {
		synchronized (messageMap) {

		if (messageMap.get(type)==null){
			messageMap.put(type, new ConcurrentLinkedQueue<>());
		//if(messageMap.get(type).isEmpty())
			//currentRobinIndex.put(type, 0);
		}
		messageMap.get(type).add(m);		
		// TODO Auto-generated method stub

	}}

	@Override
	public void subscribeBroadcast(Class<? extends Broadcast> type, MicroService m) {
		synchronized (messageMap) {
		if (messageMap.get(type)==null)
			messageMap.put(type, new ConcurrentLinkedQueue<>());
			//messageMap.put(type, new Vector<>());
		messageMap.get(type).add(m);
		
		// TODO Auto-generated method stub
		}
	}

	@Override
	public <T> void complete(Event<T> e, T result) {
		
		futureMap.get(e).resolve(result);
		futureMap.remove(e);
		
		// TODO Auto-generated method stub

	}

	@Override
	public void sendBroadcast(Broadcast b) {
		synchronized (this) {
		Queue <MicroService> m_vector = messageMap.get(b.getClass());
		if(m_vector!=null){
		Iterator<MicroService> it = m_vector.iterator();
		while (it.hasNext()){
			try {
				BlockingQueue<Message> q = queueMap.get(it.next());
				if(q!=null)
				    q.put(b);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}}}
		// TODO Auto-generated method stub

	}
	
	@Override
	public <T> Future<T> sendEvent(Event<T> e) {
		synchronized (this) {
			
		Queue<MicroService> m_vector = messageMap.get(e.getClass());
		if (m_vector==null || m_vector.isEmpty())
			return null; // null is returned if no relevant services to handle event
		//int next_robin = calcNextRobin(e);
		Future<T> future = new Future<>();
		futureMap.put(e, future);
		try {
			//if(next_robin>=0){
				queueMap.get(m_vector.peek()).put(e);
				m_vector.add(m_vector.poll());
			//}
			
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
				
		return future;
		}
	}

	//private synchronized <T> int calcNextRobin(Event<T> e) {
	//	int next = (currentRobinIndex.get(e.getClass())+1)%messageMap.get(e.getClass()).size();
//		int cur = currentRobinIndex.get(e.getClass());
//		currentRobinIndex.put(e.getClass(),next);
	//	return cur;
	//}

	@Override
	public void register(MicroService m) { 
		queueMap.put(m,new ArrayBlockingQueue<>(1000));// TODO change capacity ?
	}

	@Override
	public void unregister(MicroService m) {
		synchronized (this) {		
		BlockingQueue<Message> q = queueMap.get(m);	
		Iterator<Message> it2 = q.iterator();
		while(it2.hasNext()){
			Message msg=it2.next();		
			if (msg instanceof Event)
			    complete((Event) msg,null); 
		}
		queueMap.remove(m);
		//}
		//synchronized (messageMap){
		Iterator it1 = messageMap.entrySet().iterator();
		while(it1.hasNext())
		{
			Map.Entry<Class,Queue<MicroService>> pair = (Map.Entry)it1.next();
			//if (
			pair.getValue().remove(m);
			//{
				//break;
				//int i = pair.getValue().indexOf(m);
				//pair.getValue().remove(m);
			//	if (currentRobinIndex.get(pair.getKey())!=null && i <= currentRobinIndex.get(pair.getKey())&&pair.getValue().size()>0){ // it will be null in case of broadcast type message
					//if(currentRobinIndex.get(pair.getKey())!=0)
			//		currentRobinIndex.put((pair.getKey()),currentRobinIndex.get(pair.getKey())-1);
				//}
			//}
			if(pair.getValue().isEmpty()){
				messageMap.remove(pair.getKey());
			}
			
		}
		}

	}

	@Override
	public Message awaitMessage(MicroService m) throws InterruptedException {
		
		if (!queueMap.containsKey(m))
			throw new IllegalStateException();
		Message next_message = queueMap.get(m).take();

			//}
		return next_message;
	}
	public static MessageBusImpl getInstance(){
		if (instance == null)
			instance = new MessageBusImpl();
		return instance;
	}

}
