package bgu.spl.mics;

import bgu.spl.mics.application.passiveObjects.Customer;
import bgu.spl.mics.application.passiveObjects.OrderReceipt;

public class BookOrderEvent implements Event<OrderReceipt> {
	
    private String senderName;
    private Customer _customer;
    private String _book;
    private int ordered_tick;

    public BookOrderEvent (Customer customer, String book,int tick, String sender) {
        _customer = customer;
        _book = book;
        this.ordered_tick = tick;
        senderName = sender;
    	//this.senderName = senderName;
    }
    
    public Customer getCustomer(){
    	return _customer;
    }

    public String getSenderName() {
        return senderName;
    }

	public String getBookName() {
		return _book;
	}
	
	public int customerCredit(){
		return _customer.getAvailableCreditAmount();
	}
	public int getCustomerID(){
		return _customer.getId();
	}

	public int getOrderedTick() {
		return ordered_tick;
	}
	
	public String getCustomerAddress() {
        return _customer.getAddress();
    }
	
	public int getCustomerDistance() {
		return _customer.getDistance();
	}





}
