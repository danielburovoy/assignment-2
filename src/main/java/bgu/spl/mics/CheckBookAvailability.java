package bgu.spl.mics;

import bgu.spl.mics.application.passiveObjects.BookInventoryInfo;

public class CheckBookAvailability implements Event<Integer> {
	
	private int customer_credit;
    private String senderName;
	private String bookToCheck;

    public CheckBookAvailability(String senderName,String book,int credit) {
        this.customer_credit = credit;
    	this.senderName = senderName;
        this.bookToCheck=book;
    }

    public String getSenderName() {
        return senderName;
    }
    
    public String getBookToCheck(){
    	return this.bookToCheck;
    }
    public int getCustomerCredit(){
    	return this.customer_credit;
    }

}
